package io.arvi.arvi.connect3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    // 0 = yellow, 1 = red
    int activePlayer = 0;

    // 2 = not yet played
    int[] gameState = {2, 2, 2, 2, 2, 2, 2, 2, 2};

    // winning positions
    int[][] winningPositions = {{0,1,2}, {3,4,5}, {6,7,8}, {0,3,6}, {1,4,7}, {2,5,8}, {0,4,8}, {2,4,6}};

    // determine if game is active
    boolean gameIsActive = true;

    public void playAgain(View view) {
        // make game active again
        gameIsActive = true;

        // hide the play again layout
        LinearLayout layout = (LinearLayout)findViewById(R.id.playAgainLayout);
        layout.setVisibility(View.INVISIBLE);

        // reset game variables
        activePlayer = 0;

        // we cannot override arrays via gameState = {2, 2, 2, 2, 2, 2, 2, 2, 2}; so we have to do for loop
        for (int i = 0; i < gameState.length; i++) {
            gameState[i] = 2;
        }

        GridLayout boardLayout = (GridLayout)findViewById(R.id.boardLayout);

        // getChildCount tells us how many views inside that layout
        for (int i = 0; i < boardLayout.getChildCount(); i++) {

            // reset all image views to empty resource since we're starting a new game
            ((ImageView) boardLayout.getChildAt(i)).setImageResource(0);
        }
    }

    public void dropIn(View view) {
        ImageView counter = (ImageView) view;

        // get the android:tag value, we use it to identify the chip/element
        String tag = counter.getTag().toString();

        // convert tag to int
        int tappedCounter = Integer.parseInt(tag);

        // if the chip is not yet played, perform actions, else do nothing
        if(gameState[tappedCounter] == 2 && gameIsActive) {

            // override "not yet played" value with activePlayer tying it to the player to determine later who wins
            gameState[tappedCounter] = activePlayer;

            // animate like it is coming from the top, with a little spin (rotation)
            counter.setTranslationY(-1000f);

            if(activePlayer == 0) {
                counter.setImageResource(R.drawable.yellow);
                activePlayer = 1;
            } else {
                counter.setImageResource(R.drawable.red);
                activePlayer = 0;
            }

            counter.animate().translationYBy(1000f).rotation(360).setDuration(300);

            for (int[] winningPosition : winningPositions) {

                // if all are 1,1,1 or 0,0,0 (not 2,2,2 which means not yet played... somebody won)
                if(gameState[winningPosition[0]] == gameState[winningPosition[1]] &&
                        gameState[winningPosition[1]] == gameState[winningPosition[2]] &&
                        gameState[winningPosition[0]] != 2) {

                    gameIsActive = false;

                    String winner = "Red";

                    // override default winner
                    if(gameState[winningPosition[0]] == 0) {
                        winner = "Yellow";
                    }

                    // some one has won
                    TextView winnerMessage = (TextView)findViewById(R.id.wonTextView);
                    winnerMessage.setText(winner + " has won");


                    // make the layout appear (acts like a modal)
                    LinearLayout layout = (LinearLayout)findViewById(R.id.playAgainLayout);
                    layout.setVisibility(View.VISIBLE);
                } else {

                    // if everything is not 2 (not yet played), then it's a draw
                    boolean gameIsOver = true;

                    for(int counterState : gameState ) {

                        if(counterState == 2) {
                            // there are still chips not yet played
                            gameIsOver = false;
                        }
                    }

                    if(gameIsOver) {
                        // it's a draw
                        TextView winnerMessage = (TextView)findViewById(R.id.wonTextView);
                        winnerMessage.setText("It's a draw");


                        // make the layout appear (acts like a modal)
                        LinearLayout layout = (LinearLayout)findViewById(R.id.playAgainLayout);
                        layout.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
