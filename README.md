# The Complete Android N Developer Course
##### Section 4: Media, Images, Video and Sound
[Connect3 Game App](https://www.udemy.com/complete-android-n-developer-course/learn/v4/t/lecture/5690032)

##### Learnings
###### Animations

```
#!java
  ImageView counter = (ImageView) view;
  // animate like it is coming from the top, with a little spin (rotation)
  counter.setTranslationY(-1000f);
  counter.animate().translationYBy(1000f).rotation(360).setDuration(300);
```

###### Visibility

```
#!java
    LinearLayout layout = (LinearLayout)findViewById(R.id.playAgainLayout);
    // make the layout appear (useful as modal-like component)
    layout.setVisibility(View.VISIBLE);
    // make layout disappear
    layout.setVisibility(View.INVISIBLE);
```

###### Setting Text

```
#!java
   TextView winnerMessage = (TextView)findViewById(R.id.wonTextView);
   winnerMessage.setText("It's a draw");
```

###### Setting Image Resource
```
#!java
    ImageView counter = (ImageView) view;
    counter.setImageResource(R.drawable.yellow);
```

###### Layout Manipulation
```
#!java
    // getChildCount tells us how many views inside that layout
    for (int i = 0; i < boardLayout.getChildCount(); i++) {
        // reset all image views to empty resource since we're starting a new game
        ((ImageView) boardLayout.getChildAt(i)).setImageResource(0);
    }
```

###### Tag
```
#!java
    // get the android:tag value, we use it to identify the chip/element
    String tag = counter.getTag().toString();
```